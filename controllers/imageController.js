const request = require('request-promise-native');
const Quote = require('../models/quote');
const {createCanvas, loadImage} = require('canvas');
const fs = require('fs');
const path = require('path');
const helpers = require('../config/helpers');
const appConst = require('../config/const');
const _ = require('lodash');

const getQuoteImage = async (req, res) => {
	const quoteId = req.params.quote.replace('.jpg', '');
	const quote = await Quote.findById(quoteId).populate('author');
	const quoteImagePath = path.join(appRoot, `public/images/quotes/${quote._id}.jpg`);

	let fileExists = await helpers.checkFileExists(quoteImagePath);
	console.log(fileExists)

	if (!fileExists) {
		await createQuoteImage(quote);
		console.log(`Quote: ${quote._id} - image created`)

	}
	fileExists = await helpers.checkFileExists(quoteImagePath);
	res.sendFile(quoteImagePath);

};

const createQuoteImage = async (quote) => {
	const width = 1200;
	const height = 1200;
	const fontSize = 56;
	const lineHeight = fontSize * 1.41;
	// const quote = await getRandomQuote();
	const canvas = createCanvas(width, height);
	let tags = quote.tags.length > 0 ? '?' + quote.tags.join(',') : '';
	if (!tags) {
		let words = _.words(quote.text.toLowerCase());
		words = words.filter(item => item.length > 4);
		if (words.length > 0) {
			tags = '?' + words.slice(0, 5).join(',');
		}
	}
	console.log(tags);
	const imageRedirectResponse = await request.get({url: `https://source.unsplash.com/${width}x${height}/${tags}`, followRedirect: false, resolveWithFullResponse: true, simple: false});
	const profession = appConst.professions.find(item => item.slug === quote.author.profession);

	const ctx = canvas.getContext('2d');
	const image = await loadImage(imageRedirectResponse.headers.location);
	ctx.drawImage(image, 0, 0);

	ctx.fillStyle = 'rgba(0, 0, 0, 0.6)';
	ctx.fillRect(0, 0, canvas.width, canvas.height);


	function wrapText(context, text, lastLine, x, y, maxWidth, maxHeight, lineHeight) {
		let words = text.split(' ');
		let line = '';
		let top = maxHeight / 2 - (text.length / (fontSize/3.2)) * lineHeight

		if (top > 0) {
			y = y + top;
		}

		for (let n = 0; n < words.length; n++) {
			let testLine = line + words[n] + ' ';
			let metrics = context.measureText(testLine);
			let testWidth = metrics.width;
			if (testWidth > maxWidth && n > 0) {
				context.fillText(line, x, y);
				line = words[n] + ' ';
				y += lineHeight;
			} else {
				line = testLine;
			}
		}
		context.fillText(line, x, y);

		context.font = `italic ${fontSize}px Open Sans`;
		context.fillText(lastLine, x, y + lineHeight * 2);
	}


	// ctx.font = '48px Open Sans';
	// ctx.fillStyle = "white";
	// wrapText(ctx, quote.text, 50, 80, 1100, 500, 64);
	//
	// ctx.font = 'italic 48px Open Sans';
	// ctx.fillText('— ' + quote.author.name + ', ' + profession.name, 50, 570);
	//
	// ctx.font = 'italic 18px Open Sans';
	// ctx.fillText('99quotes.me', 1030, 570);


	ctx.font = `${fontSize}px Open Sans`;
	ctx.fillStyle = "white";
	wrapText(ctx, quote.text, '— ' + quote.author.name + ', ' + profession.name, fontSize, lineHeight*3, width - fontSize * 2, height - fontSize * 2, lineHeight);

	// ctx.font = 'italic 64px Open Sans';
	// ctx.fillText('— ' + quote.author.name + ', ' + profession.name, 50, height - 60);

	ctx.font = 'italic 24px Open Sans';
	ctx.fillText('99quotes.me', width - 200, height - fontSize);

	const stream = canvas.createJPEGStream();
	const out = fs.createWriteStream(`./public/images/quotes/${quote._id}.jpg`);


	const end = (stream, out) => new Promise((resolve, reject) => {
		stream.pipe(out);
		stream.on('end', resolve);
		stream.on('error', reject); // or something like that. might need to close `hash`
	});
	console.log(1)
	await end(stream, out);
	console.log(2)
	// res.json('ok')
};


module.exports = {
	getQuoteImage
};


