const Quote = require('../models/quote');
const Author = require('../models/author');
const appConst = require('../config/const');
const helpers = require('../config/helpers');
const path = require('path');
const _ = require('lodash');

const index = async (req, res) => {
	const randomQuote = (await Quote.aggregate([
		{$sample: {size: 1}}, {
			$lookup: {
				from: "authors",
				localField: "author",
				foreignField: "_id",
				as: "author"
			}
		}, {$unwind: '$author'},
	]))[0];

	const quoteImagePath = path.join(appRoot, `public/images/quotes/${randomQuote._id}.jpg`);
	randomQuote.hasImage = await helpers.checkFileExists(quoteImagePath);

	res.render('quote/index', {
		quote: randomQuote,
		title: 'Popular, Motivational & Inspirational Quotes by 20 000+ Authors',
		description: 'Quotes about love, life, motivation, success, time, friends, family and more. Find your favorite celebrity and inspire by quotes!',
		canonical: '/',
	})
};

const getAuthorsByLetter = async (req, res) => {
	const authors = await Author.find({letter: req.params.letter});
	res.render('author/letter', {
		authors: authors,
		letter: req.params.letter,
		title: `Popular, Motivational & Inspirational Quotes. ${authors.length} Authors on «${req.params.letter.toUpperCase()}»`,
		description: `Quote authors on letter ${req.params.letter.toUpperCase()}. Quotes about love, life, motivation, success, time, friends, family and more. Inspire by quotes!`,
		canonical: `/authors/${req.params.letter}`,
	});
};

const getAuthorsByProfession = async (req, res) => {
	const profession = appConst.professions.find(item => item.slug === req.params.slug);
	const authorsByLetters = await Author.aggregate([
		{$match: {profession: req.params.slug}},
		{$group: {_id: "$letter", authors: {$push: "$$ROOT"}}},
	]).sort('_id');
	// res.json(authorsByLetters);
	// const authors = await Author.find({profession: req.params.slug});
	res.render('author/profession', {
		authorsByLetters: authorsByLetters,
		profession: req.params.slug,
		title: `Famous ${profession.name}s (Quote Authors) - 99quotes.me`,
		description: `Find your favorite ${profession.name}s and inspire by their quotes! Choose your favorite authors!`,
		canonical: `/professions/${req.params.slug}`,
	});
};

const searchAuthors = async (req, res) => {
	let query = req.query.q.trim();
	let authors = [];
	if (query.length > 1) {
		const regex = new RegExp(query, 'i');
		authors = await Author.find({name: regex}).sort('name');
	}
	res.render('search', {
		authors: authors, q: query,
		title: 'Search Your Favorite Quote Authors on 99quotes.me Now',
		description: `Find your favorite quotes by author name. Quotes about love, life, motivation, success, time, friends, family and more. Inspire by this quotes now!`,
		canonical: `/search`,
	});
};


const getQuotesByAuthor = async (req, res) => {
	const author = await Author.findOne({slug: req.params.author});
	const quotes = await Quote.find({author: author}).populate('author');
	const tags = [];
	for (let quote of quotes) {
		const quoteImagePath = path.join(appRoot, `public/images/quotes/${quote._id}.jpg`);
		quote.hasImage = await helpers.checkFileExists(quoteImagePath);

		for (let topicSlug of quote.tags) {
			const topic = appConst.topics.find(item => item.slug === topicSlug);
			if (topic) {
				if (tags.indexOf(topic) === -1) {
					topic.count = 1;
					tags.push(topic);
				} else {
					topic.count++;
				}
			}
		}
	}

	tags.sort((a, b) => {
		if (a.count > b.count) return -1;
		if (a.count < b.count) return 1;
		return 0;
	});

	const tagNames = tags.map(item => _.capitalize(item.name));
	let title = `TOP-${quotes.length > 100 ? 100 : quotes.length} Best ${author.name} Quotes: ${tagNames.slice(0, 4).join(', ').replaceLast(', ', ' & ')}`;
	if (title.length > 68) title = `TOP-${quotes.length > 100 ? 100 : quotes.length} Best ${author.name} Quotes: ${tagNames.slice(0, 3).join(', ').replaceLast(', ', ' & ')}`;
	if (title.length > 68) title = `TOP-${quotes.length > 100 ? 100 : quotes.length} Best Inspirational ${author.name} Quotes`;
	if (title.length > 68) title = `TOP-${quotes.length > 100 ? 100 : quotes.length} Best ${author.name} Quotes`;

	res.render('author/show', {
		author: author,
		quotes: quotes,
		tags: tags,
		title: title,
		description: `Find your favorite ${author.name} quotes about ${tagNames.slice(0, 6).join(', ')} and more!. Inspire by ${author.name} quotes now!`,
		canonical: `/quotes/${author.slug}`,
	});
};

const getQuotesByAuthorAndTag = async (req, res, next) => {
	const topic = appConst.topics.find(item => item.slug === req.params.topic);
	if (!topic) {
		next();
		return;
	}
	const author = await Author.findOne({slug: req.params.author});
	const quotes = await Quote.find({author: author, tags: req.params.topic}).populate('author');

	for (let quote of quotes) {
		const quoteImagePath = path.join(appRoot, `public/images/quotes/${quote._id}.jpg`);
		quote.hasImage = await helpers.checkFileExists(quoteImagePath);
	}

		let title = `${quotes.length > 2 ? 'TOP-' + quotes.length + ' ' : 'The '}Best Inspirational ${author.name} Quotes About ${_.capitalize(topic.name)}`;
	if (title.length > 68) title = `${quotes.length > 2 ? 'TOP-' + quotes.length + ' ' : 'The '}Best ${author.name} Quotes About ${_.capitalize(topic.name)}`;

	res.render('author/topic', {
		author: author,
		quotes: quotes,
		topic: topic,
		title: title,
		description: `Find your favorite ${author.name} quotes about ${topic.name}. Inspire by ${author.name} quotes now! Quotes about ${topic.name} and more!`,
		canonical: `/quotes/${author.slug}/${topic.slug}`,
	});
};

const getQuotesByTopic = async (req, res, next) => {
	const topic = appConst.topics.find(item => item.slug === req.params.topic);
	if (!topic) {
		next();
		return;
	}
	const page = parseInt(req.query.page) || 1;

	const quotes = await Quote
		.find({tags: req.params.topic})
		.populate('author')
		.skip((page - 1) * appConst.pageSize)
		.limit(appConst.pageSize);

	for (let quote of quotes) {
		const quoteImagePath = path.join(appRoot, `public/images/quotes/${quote._id}.jpg`);
		quote.hasImage = await helpers.checkFileExists(quoteImagePath);
	}

	const nextPage = quotes.length > 0 ? page + 1 : 0;
	res.render('quote/topic', {
		quotes: quotes,
		topic: topic,
		nextPage: nextPage,
		title: `TOP-100 Best Inspirational & Motivational Quotes About ${_.capitalize(topic.name)}`,
		description: `Find the best inspirational and motivational quotes about ${topic.name} of all time. Inspire by ${topic.name} quotes on 99quotes now!`,
		canonical: `/quotes/${topic.slug}`,
	});
};


const getQuotesByProfession = async (req, res) => {
	const page = parseInt(req.query.page) || 1;
	const profession = appConst.professions.find(item => item.slug === req.params.slug);
	const quotes = await Quote.find({profession: req.params.slug}).populate('author').sort('id').limit(appConst.pageSize).skip((page - 1) * appConst.pageSize);

	for (let quote of quotes) {
		const quoteImagePath = path.join(appRoot, `public/images/quotes/${quote._id}.jpg`);
		quote.hasImage = await helpers.checkFileExists(quoteImagePath);
	}

	const nextPage = quotes.length > 0 ? page + 1 : 0;
	res.render('quote/profession', {
		quotes: quotes,
		profession: req.params.slug,
		nextPage: nextPage,
		title: `TOP-100 Best Inspirational & Motivational ${profession.name}s Quotes`,
		description: `Find the best inspirational and motivational ${profession.name}'s quotes of all time. Quotes about love, life, motivation, success, time, friends, family and more!`,
		canonical: `/professions/${req.params.slug}/quotes`,
	});
};

const getQuote = async (req, res) => {
	const id = req.params.id.replace('q-', '');
	const quote = await Quote.findById(id).populate('author');

	const quoteImagePath = path.join(appRoot, `public/images/quotes/${quote._id}.jpg`);
	const hasImage = await helpers.checkFileExists(quoteImagePath);
	quote.hasImage = hasImage;

	let title = `${quote.author.name} — `;
	const maxLen = 65 - title.length;
	let titleQuote = quote.text.substring(0, maxLen);
	titleQuote = quote.text.substring(0, titleQuote.lastIndexOf(' '));
	title = title + titleQuote + '...';
	res.render('quote/show', {
		quote: quote,
		title: title,
		hasImage: hasImage,
		metaImage: `/quote-images/${quote._id}.jpg`,
		description: `${quote.text} — ${quote.author.name}`,
		canonical: `/quotes/${quote.author.slug}/q-${quote._id}`,
	});
};

const getProfessions = async (req, res) => {
	res.render('page/professions', {
		title: 'Quote Authors sorted by Professions',
		description: `Find your favorite quotes by author profession. Inspirational quotes about love, life, motivation, success, time, friends, family and more!`,
		canonical: `/professions`,
	})
};
const getTopics = async (req, res) => {
	res.render('page/topics', {
		title: 'Find The Best Inspirational Quotes by Your Favorite Topics Now',
		description: `Find your favorite quotes by topic. Inspirational quotes about love, life, motivation, success, time, friends, family and more!`,
		canonical: `/topics`,
	})
};

module.exports = {
	index,
	getAuthorsByLetter,
	getAuthorsByProfession,
	getQuotesByProfession,
	getQuotesByAuthor,
	getQuotesByAuthorAndTag,
	getQuotesByTopic,
	getQuote,
	searchAuthors,
	getProfessions,
	getTopics,
};

String.prototype.replaceLast = function (find, replace) {
	let lastIndex = this.lastIndexOf(find);

	if (lastIndex === -1) {
		return this;
	}

	let beginString = this.substring(0, lastIndex);
	let endString = this.substring(lastIndex + find.length);

	return beginString + replace + endString;
};
