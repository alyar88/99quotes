const cron = require('node-cron');

const telegramController = require('../controllers/social/telegramController');
const tumblrController = require('../controllers/social/tumblrController');
const twitterController = require('../controllers/social/twitterController');
const pinterestController = require('../controllers/social/pinterestController');

cron.schedule('37 */4 * * *', async () => {
	console.log('Start posting every 4 hours');
	await telegramController.postRandomQuote();
	await tumblrController.postRandomQuote();
	await twitterController.postRandomQuote();
	await pinterestController.postRandomQuote();
	console.log('End posting every 4 hours');
});
