const Quote = require('../models/quote');
const Author = require('../models/author');
const appConst = require('../config/const');

const updateQuoteProfession = async (req, res) => {
	const cursor = Quote.find().populate('author').cursor();
	let quote;
	while (quote = await cursor.next()) {
		quote.profession = quote.author.profession;
		await quote.save();
	}
};

const updateAuthorSlug = async (req, res) => {
	const cursor = Author.find().cursor();
	let author;
	while (author = await cursor.next()) {
		console.log(author.slug, author.slug.replace(/\_/mg, '-'));
		author.slug = author.slug.replace(/\_/mg, '-');
		await author.save();
	}
};

const updateQuoteTags = async (req, res) => {
	console.log(`Clean all topics`);
	await Quote.updateMany({}, {tags: []});
	console.log(`Clean all topics complete`);
	for (let i = 0; i < appConst.topics.length; i++) {
		const topic = appConst.topics[i];

		console.log(`Topic ${topic.name} update start`)

		let terms = '';
		topic.terms.forEach(term => {
			terms = terms + '\\' + term + '\\ '
		});
		const quotes = await Quote.find({$text: {$search: terms}});

		for (let j = 0; j < quotes.length; j++) {
			const quote = quotes[j];
			quote.tags.push(topic.slug);
			await quote.save();
		}

		console.log(`Topic ${topic.name} updated`)

	}


};

module.exports = {
	updateQuoteProfession,
	updateAuthorSlug,
	updateQuoteTags
};
