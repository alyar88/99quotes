const express = require('express');
const router = express.Router();
const braiyQuoteController = require('../controllers/braiyQuoteController');
const quoteController = require('../controllers/quoteController');
const updateController = require('../controllers/updateController');
const wikipediaController = require('../controllers/wikipediaController');
const sitemapController = require('../controllers/sitemapController');
const imageController = require('../controllers/imageController');
const telegramController = require('../controllers/social/telegramController');
const tumblrController = require('../controllers/social/tumblrController');
const twitterController = require('../controllers/social/twitterController');
const pinterestController = require('../controllers/social/pinterestController');

/* GET home page. */
// router.get('/', function(req, res, next) {res.json('ok');});
// router.get('/', function(req, res, next) {
// 	res.render('index', { title: 'Express' });
// });

router.get('/', quoteController.index);
router.get('/professions', quoteController.getProfessions)
router.get('/topics', quoteController.getTopics)
router.get('/authors/:letter([a-z])', quoteController.getAuthorsByLetter);
router.get('/professions/:slug', quoteController.getAuthorsByProfession)
router.get('/professions/:slug/quotes', quoteController.getQuotesByProfession)
router.get('/quotes/:topic', quoteController.getQuotesByTopic)
router.get('/quotes/:author', quoteController.getQuotesByAuthor);
router.get('/quotes/:author/:topic', quoteController.getQuotesByAuthorAndTag);
router.get('/quotes/:author/:id(q-*)', quoteController.getQuote);
router.get('/search', quoteController.searchAuthors);


router.get('/sitemap.xml', sitemapController.sitemap);


router.get('/quote-images/:quote(*\.jpg)', imageController.getQuoteImage);

router.get('/telegram/random-quote', telegramController.postRandomQuote);
router.get('/tumblr/random-quote', tumblrController.postRandomQuote);
router.get('/twitter/random-quote', twitterController.postRandomQuote);
router.get('/pinterest/random-quote', pinterestController.postRandomQuote);



router.get('/data/professions', braiyQuoteController.getProfessions);
router.get('/data/authors', braiyQuoteController.getAuthors);
router.get('/data/authors-update', braiyQuoteController.updateAuthors);
router.get('/data/quotes', braiyQuoteController.getQuotes);
router.get('/data/update/quotes-profession', updateController.updateQuoteProfession);
router.get('/data/update/author-slug', updateController.updateAuthorSlug);
router.get('/data/update/quote-tags', updateController.updateQuoteTags);
router.get('/data/wikipedia/author-image', wikipediaController.getImages);

module.exports = router;

