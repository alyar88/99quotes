const fs = require('fs');
const Quote = require('../models/quote');


const checkFileExists = s => new Promise(r => fs.access(s, fs.F_OK, e => r(!e)));

const getRandomQuote = async () => {
	return (await Quote.aggregate([
		{$sample: {size: 1}}, {
			$lookup: {
				from: "authors",
				localField: "author",
				foreignField: "_id",
				as: "author"
			}
		}, {$unwind: '$author'},
	]))[0];
};

module.exports = {
	checkFileExists,
	getRandomQuote,
};

