const hbs  = require('hbs');
const appConst = require('../config/const');

hbs.registerHelper('profName', (slug) => {
	const prof = appConst.professions.find(item => item.slug === slug);
	return prof ? prof.name : '';
});

hbs.registerHelper('trimByWords', (text, symbols) => {
	let result = text.substring(0, symbols);
	result = text.substring(0, result.lastIndexOf(' '));
	return result;
});
