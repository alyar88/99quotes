const Quote = require('../../models/quote');
const helpers = require('../../config/helpers');
const TelegramBot = require('node-telegram-bot-api');
const bot = new TelegramBot(process.env.TELEGRAM_BOT_TOKEN)

const postRandomQuote = async (req, res) => {
	const quote = await helpers.getRandomQuote();
	const imageUrl = `https://99quotes.me/quote-images/${quote._id}.jpg`;

	await bot.sendPhoto('@the99quotes', imageUrl, {
		parse_mode: 'html',
		caption:`${quote.text}\n— <a href="https://99quotes.me/quotes/${quote.author.slug}">${quote.author.name}</a>`
	});
	res && res.json('ok');
};

module.exports = {
	postRandomQuote
};
