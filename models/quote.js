const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const quoteSchema = new Schema({
	text: String,
	id: {type: Number, unique: true},
	author: {type: Schema.Types.ObjectId, ref: 'Author'},
	profession: {type: String, index: true},
	tags: [String],
});

quoteSchema.index({ text: 'text' });

const Quote = mongoose.model('Quote', quoteSchema);

module.exports = Quote;
9
