const sm = require('sitemap');
const appConst = require('../config/const');
const Author = require('../models/author');

const sitemap = async (req, res) => {
	const sitemap = sm.createSitemap({
		hostname: 'https://99quotes.me',
		cacheTime: 600000,
	});

	sitemap.add({url: '/', priority: 0.8, changefreq: 'always'});

	for (let i = 0; i < appConst.topics.length; i++) {
		sitemap.add({url: '/quotes/' + appConst.topics[i].slug, priority: 0.7});
	}
	for (let i = 0; i < appConst.professions.length; i++) {
		sitemap.add({url: '/professions/' + appConst.professions[i].slug, priority: 0.3});
	}

	for (let i = 0; i < appConst.professions.length; i++) {
		sitemap.add({url: '/professions/' + appConst.professions[i].slug + '/quotes', priority: 0.7});
	}

	const cursor = Author.find().cursor();
	let author;
	while (author = await cursor.next()) {
		sitemap.add({url: '/quotes/' + author.slug, priority: 0.5});
	}

	res.header('Content-Type', 'application/xml');
	res.send( sitemap.toString() );
};

module.exports = {
	sitemap
};
