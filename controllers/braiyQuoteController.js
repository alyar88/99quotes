const request = require('request-promise-native');
const cheerio = require('cheerio');
const Author = require('../models/author');
const Quote = require('../models/quote');
const appConst = require('../config/const');

const getQuotes = async (req, res) => {
	const cursor = Author.find({p: {$exists: false}},{}, {timeout: true}).cursor();
	let author;
	while (author = await cursor.next()) {
		let page = 1;
		let pageExists = true;
		let pageSize = 0;
		do {
			const url = `https://www.brainyquote.com/authors/${author.slug}${page > 1 ? ('_' + page) : ''}`;
			try {
				const $ = cheerio.load(await request.get({url: url, followRedirect: false}));
				await $('div[id].boxy.bqQt.grid-item').each(async (i ,node) => {
					const text = $(node).find('a.b-qt.oncl_q').text().trim();
					const id = $(node).find('span.favAddToCol').attr('data-ng-class').replace('collectionCssClass(', '').replace(')', '');

					let quote = await Quote.findOne({id: id});
					if (!quote) quote = new Quote();
					quote.text = text;
					quote.id = id;
					quote.author = author;

					await quote.save();
					console.log(text.substring(0, 25), id, author.name);
				});

				pageSize = $('div[id].boxy.bqQt.grid-item').length;
				console.log('Page Size:', pageSize);

			} catch (e) {
				// console.error(e);
				pageExists = false;
				pageSize = 0;
				console.log(`Pages count: ${page - 1}`)
			}
			console.log(url)
			page++;
			// sleep.msleep(100);
		} while (pageSize >= 26)
		author.p = true;
		await author.save();
	}
};

const updateAuthors = async (req, res) => {
	const cursor = Author.find({},{}, {timeout: true}).cursor();
	let author;
	while (author = await cursor.next()) {
		author.slug = author.slug.replace('/authors/', '');
		await author.save();
	}
};

const getAuthors = async (req, res) => {
	const letters = appConst.alphabet;

	await asyncForEach(letters, async letter => {
		let page = 1;
		let pageExists = true;
		do {
			const url = `https://www.brainyquote.com/authors/${letter}${page > 1 ? page : ''}`;
			try {
				const $ = cheerio.load(await request.get({url: url, followRedirect: false}));
				await $('table.table tbody tr').each(async (i ,node) => {
					const name = $(node).find('td a').text().trim();
					const slug = $(node).find('td a').attr('href').replace('/authors/', '');
					const professionName = $(node).find('td:nth-child(2)').text().trim();
					const profession = appConst.professions.find(item => item.name === professionName);
					const professionSlug = profession ? profession.slug : false;
					console.log(name, slug, professionSlug);
					let author = await Author.findOne({slug: slug});
					if (!author) author = new Author();
					author.name = name;
					author.slug = slug;
					author.profession = professionSlug;
					author.letter = letter;

					await author.save();
				});

			} catch (e) {
				pageExists = false;
				console.log(`Pages count: ${page - 1}`)
			}
			console.log(url)
			page++;
		} while (pageExists)

	});

};

const getProfessions = async (req, res) => {
	const $ = cheerio.load(await request.get('https://www.brainyquote.com/profession/'));
	const list = [];
	await $('.bqLn a.topicIndexChicklet').each((i, node) => {
		list.push({
			name: $(node).text().trim(),
			slug: $(node).attr('href').replace('/profession/', '').replace('_quotes', ''),
		})
	});
	res.json(list);
};


module.exports = {
	getProfessions,
	getAuthors,
	getQuotes,
	updateAuthors,
};
