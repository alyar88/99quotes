const request = require('request-promise-native');
const image2base64 = require('image-to-base64');
const Quote = require('../../models/quote');
const helpers = require('../../config/helpers');
const Twitter = require('twit');
const client = new Twitter({
	consumer_key: 'jsHSgqOon7oImkiPontEWFV05',
	consumer_secret: '4fkhdXgWE8HVviKUOcy3s6YEdBfK70nvuag9lnhEfbRpYCABh0',
	access_token: '1075359252666114048-incm7K3Qv0DV2fzqEQVhrzQdtueRrJ',
	access_token_secret: 'Qy5WvEciO2eUwamTlqQHXXOy34U3hCZLdkwhltACMyIVv'
});

const postRandomQuote = async (req, res) => {
	const quote = await helpers.getRandomQuote();
	const imageUrl = `https://99quotes.me/quote-images/${quote._id}.jpg`;
	const quoteUrl = `https://99quotes.me/quotes/${quote.author.slug}`;
	let tags =`#quote #quotes #${quote.author.name.replace(/\s/g,'')} #${quote.author.profession.replace(/\s/g,'')} ${quote.tags.length > 0 ? '#' + quote.tags.join(' #') : ''}`;

	let tweetText = `${quote.text} — ${quote.author.name} ${tags} ${quoteUrl}`;
	if (tweetText.length > 280) {
		let tweetText = `${tags} ${quoteUrl}`;
	}

	const image = await image2base64(imageUrl);

	const media = await client.post('media/upload', {media_data: image});
	await client.post('statuses/update', {status: tweetText, media_ids: media.data.media_id_string});

	res && res.json('ok')
};

module.exports = {
	postRandomQuote
};
