const mongoose = require("mongoose");
mongoose.promise = global.Promise;
mongoose.connect(
	"mongodb://localhost:27017/" + (process.env.DB_NAME || 'quotes'),
	{
		useNewUrlParser: true,
		useCreateIndex: true,
	}
);
