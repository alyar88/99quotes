const PDK = require('node-pinterest');
const _ = require('lodash');
const helpers = require('../../config/helpers');
const appConst = require('../../config/const');
const pinterest = PDK.init('AqyTiLBcncDl3LWrmUJ7BSADzCBpFXMK-8j6hiJFfRu69OBl2AgZADAAAehQRX0gKyigUZYAAAAA');
// pinterest.api('me').then(console.log);

const postRandomQuote = async (req, res) => {
	const quote = await helpers.getRandomQuote();
	const imageUrl = `https://99quotes.me/quote-images/${quote._id}.jpg`;
	const quoteUrl = `https://99quotes.me/quotes/${quote.author.slug}`;
	const profession = appConst.professions.find(item => item.slug === quote.profession).name;
	const boards = (await pinterest.api('me/boards')).data;

	let boardName = 'My Favorite Quotes';

	if (quote.tags.length > 0) {
		const topic = appConst.topics.find(item => item.slug === quote.tags[0]);
		boardName = 'Quotes About ' + _.capitalize(topic.name);
	}

	let board = boards.find(item => item.name.toLowerCase() === boardName.toLowerCase());

	if (!board) {
		board = (await pinterest.api('boards', {method: 'POST', body: {name: boardName}})).data;
		console.log(`Board ${board.name} created!`)
	}

	await pinterest.api('pins', {
		method: 'POST',
		body: {
			board: board.id,
			note: `${quote.text} — ${quote.author.name}, ${profession}`,
			link: quoteUrl,
			image_url: imageUrl,
		}
	});
	res && res.json('ok')

};

module.exports = {
	postRandomQuote
};
