const request = require('request-promise-native');
const path = require('path');
const download = require('image-downloader');
const cheerio = require('cheerio');
const Author = require('../models/author');

const getImages = async (req, res) => {
	const cursor = Author.find({image: {$exists: false}}).cursor();
	const imgPath = path.join(appRoot, 'public/images/authors/');
	let author;
	let startParse = false;
	while (author = await cursor.next()) {
		if (author.letter === 'w') startParse = true;
		if (startParse) {
			const url = encodeURI(`https://en.wikipedia.org/w/index.php?search=${author.name}`);
			try {
				const $ = cheerio.load(await request.get({url: url}));
				const metaImage = $('meta[property="og:image"]');
				if (metaImage && metaImage.length > 0) {

					const imgUrl = metaImage.attr('content');
					let imgExtention = imgUrl.substring(imgUrl.lastIndexOf('.')).toLowerCase();
					if(imgExtention.length > 5) imgExtention = '.jpg';
					const imgName = author.slug + imgExtention;

					await download.image({url: imgUrl, dest: imgPath + imgName});
					author.image = imgName;
					await author.save();
					console.log(`${author.name} - ${imgName}`)
				} else {
					console.log(`${author.name} - NO image, no meta`)
				}
			} catch (e) {
				console.log(`${author.name} - NO image, wikipedia error`)
			}
			console.log(url)
		}
	}
};


module.exports = {
	getImages,
};
