const tumblr = require('tumblr.js');
const helpers = require('../../config/helpers');
const client = tumblr.createClient({
	consumer_key: '9ammbUCXkQj46Fg291VXcoLfqxYpV3zGLEOW4TknDdhkkY3w9b',
	consumer_secret: 'EUPhoWIEfJJtzwxmeeQJaMox9cuOntxuOXbi2ly3KwBG1ZgsIp',
	token: 'RAAKlOcqYN4uk6ZjqN7Djb9esY8aUJfrUtic4VO3oAusz1xIjO',
	token_secret: 'GxOZG9Xf4GJA1hJNj9gkjsgWWzdbNr8NyeI6mZK4bNXKJOnpTy'
});

const postRandomQuote = async (req, res) => {
	const quote = await helpers.getRandomQuote();
	const imageUrl = `https://99quotes.me/quote-images/${quote._id}.jpg`;
	const quoteUrl = `https://99quotes.me/quotes/${quote.author.slug}`;

	await client.createLinkPost('99quotes.tumblr.com', {
		url: quoteUrl,
		tags: (`quote,${quote.author.name},${quote.author.profession},${quote.tags.join(',')}`).replace(/\s/g,''),
		thumbnail: imageUrl,
		title: `Quote by ${quote.author.name}`,
		description: `${quote.text}\n—${quote.author.name}`,
		excerpt: quote.text
	}, () => {
		res && res.json('ok');
	});

};

module.exports = {
	postRandomQuote
};
