const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const authorSchema = new Schema({
	name: {type: String},
	slug: {type: String, index: true, unique: true},
	profession: {type: String, index: true},
	nationality: String,
	letter: String,
	p: Boolean,
	image: String,
}, {
	toObject: {
		getters: true
	}, toJSON: {
		getters: true
	}
});

authorSchema.index({ name: 'text' });

const Author = mongoose.model('Author', authorSchema);

module.exports = Author;
